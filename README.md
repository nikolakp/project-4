<h1> Project 4 <h1>

# Table of contents

1. [Discussion and explanation]
   1. [Technology]
      1. [React and Typescript]
      2. [REST API, Database and Datasets]
      3. [External Libraries]
   2. [Decisions]
      1. [Presentations and Design]
      2. [User Interaction]
2. [Testing]
3. [Structure]
4. [How to Run]
5. [Sources]

# Short version and Introduction

Project 4 has reused most of the code from Project 3 (https://gitlab.stud.idi.ntnu.no/it2810-h20/team-63/project-3/), and will be used as a reference throughout the readme. The project is created as a react-native app that copies the behavior from project-3 in http://it2810-28.idi.ntnu.no/.
This is due to the nature of the task and the backend is not needed changing. Moreover, most of the frontend was also reusable and only needed some convertatoin for instance in the return statements (Div  View). Other notable changes were the styling. CSS is not supported, and the code was redesigned using react nativs stylesheets. Variables, components and functions have mostly kept their old names. Some changes in the frontend structure due to the design changes needed for mobile devices. Furthermore, the readme has been reused and refined to fit the project. Bootstrap is also not supported and the need for new third-party components was again relevant.

Still, after changes in layout and design, the mobile version of the application still includes a search interface that provides a result set and is loaded dynamically by “Load more movies…”. It must be possible to get a detailed view of each object and it supports the same types of interactiv change to the search results. Some components are not in used as the solution was to initally to be implemented for a android system as well as for iOS. In lack of a android system this was solution was abandod.

# Discussion and Explanation

This part will discuss, explain and show all the main choices and solutions that the group did. The first part will discuss the technological tools that were used, followed by a section that explains the most important choices and design.

## Technology

### React and Typescript

As mentioned, this project further builds on the earlier submitted project 3. The result is a mobile application built with react native, expo and typescript. The project is initialized with "expo init", using the Typescript template and the development tool is set triggered using "expo-cli". By using the terminal command «expo start», the client is initialized, and the Metro Bundler starts running. Expo's mobile application will then be hosted locally if units are using the same network. The project is based Typescript, which can be seen on GitLabs' "Repository Analytics".
React Native is used for frontend development accompanied by typescript

Javascript was used for backend development. The group sees the advantages of using Typescript in the backend as well, since this would add a testing of correct types before compiling the code. Also the external libraries work well with Typescript.

### REST API, Database and Datasets

The dataset is extracted from [_Kaggle_](https://www.kaggle.com/prateekmaj21/disney-movies) by [Prateek Majumder](https://www.kaggle.com/prateekmaj21). The dataset includes only 537 items, but the project is set up so that it is scalable and can handle larger datasets. It is also stripped and cleaned up by removing everything except id, movie_title, release_date and genre. The two attributes: number_of_ratings and rating are added and the value is decided by the users. For the communication between the server and client the group created a Representational State Transfer Application Programming Interface (REST API) with Node-js, Express and MongoDB.

MongoDB is used as the backend solution and is installed into the virtual machine. It is mainly used since the group is familiar with its pros from other courses. The database provider enables the possibility to build easily, adapt quickly, and scale reliably. The database is drifted by the group and since it is a JSON document-oriented database it has been easy to collect the correct information.

Insomnia Core was used to check the backend routes and queries during development. To visually explore the data MongoDB Compass was used. This graphical user interface allowed the developers to run ad hoc queries in real time. The built-in visualization was an important tool to have control over the data that was used on the website and to actively interact and understand the data stored in MongoDB Database.

### External Libraries

Due to unsupported old external libraries a new research had to be done. React Native imports such as View, Text, StyleSheet, ScrollView Image, Platform React Native Elements, was used as shortcut for clean design and other solutions. The components that were imported was also used as an advantage for a structured layout creating a well human and machine integration.
Further some react-native-elements was imported. The cross platform React native UI toolkit have pre-build components to help developers with typical reusable code. Button, Overlay, SearchBar, Card, Text, AirbnbRating and CheckBox are frequently used to increase the ease of use and design.

React redux is used as a convenient state container. This tool simplifies storing and managing the component states in the application. In the solution, redux is used to store all movie states and the query states in a single object and allowing other components to access the state without dealing with child components or using callbacks. The result has been that the application is more manageable and easier to test and log data changes. The group wanted to implement a scalable website that can handle larger datasets, the decision landed on using Redux instead of Mobx. This is because Redux is more applicable for large applications.
Axios was used to fetch the data from the API. The axios function that is imported and used with the .get() or .post()-function, that allows respectively to either read or overwrite. The response is then saved using redux. One key advantage with axios was that it provided automatic JSON serialization and parsing.

After the React update (16.8 Hooks) it became possible to use state in functional components, and thus did not see the need to implement classes. React hooks allowed the group to reuse stateful logic without changing your component hierarchy and made it easy to share these Hooks among many components. This was used to save search, filter and sort states before the user presses the “Show result” button.

## Decisions

### Presentations and Design

The application is, in its mobile version, only showing one column of movies. The reason to not try and include to many and therefore small objects on a small screen. Same as before, the first page the gallery or library presents 9 movies. This number is increased with the same amount each click on the button “Load more movies…”. This is done, so that only parts of the data are loaded for each click press. This will reduce the runtime and make the website scalable for large datasets.

The main change from the web edition to the phone edition is that the button “Refine search” has to be chosen to be able to make changes and customize what is shown in the result set. This is done, as it is a typical design decision that user are used to from other user interfaces. The searchbar is included both for the modal or overlay since this is the main feature in any libraries or catalogues. The user should be able to understand the point with the webpage from the first second they are introduced to it. Further, by inspiration from group 63, the pop-up overlay idea makes it easy for the user to see the options without using the whole area of the screen. Two scrollviews are introduced, and typical checkboxes are very self-explainable. With text and headers there should be no misunderstandings when navigating through the application. The user friendliness have beenin focus when developing and changing the layout for a smaller screen.
The colors and fonts are chosen with care and collaborate well with the Disney features. The header font is placed in assets and is added through third party components. When choosing “See more” a representation is shown with the use of the library component Overlay. The attributes presented give relevant information to the user. The interface has a clean interface that makes young users understand how to navigate on the webpage.

### User Interaction

A user is able to sort based on the title from A to Z and Z to A. Furthermore, the user can sort on the user generated ratings. This is a typical sorting implementation as the user normally would like to see movies with high user ratings or to sort on the title to find a movie that they can't remember the name of. Filtering is also implemented; this gives the user the ability to only show movies with certain genres. A search bar was also implemented, as this is a “must have” feature in every online library. To apply the filter, sort and search the “Show results” button must be pressed to activate the refined search and show the results. This is designed intentionally with inspiration from Finn.no. The “Show results”-button will then show the 9 first movies that fit the refined search. A user does not have to apply all three features when pressing “Show results”. The search, filter and sort states are saved using hooks in. When the “show result” button is pressed, the new path with these states are globally saved using the reducer, so they can be used again in the “Load more” button. Sorting, search and filtering is done with a query in the backend, so that the whole dataset is sorted, searched and filtered, not just the loaded movies.

The user is able to give a rating for each movie on a scale from 1 to 10. The overall rating will update and show the average of all user ratings. The number of ratings will also increase. An info text will appear to let the user know that the rating was successful. User rating gives the user an extra element of involvement, by having the user actually change the datasets in the database and able to instantly see the change. As the project description did not demand a log-in and profile implementation the group decided to give each user and web session unlimited ratings. This is something that could have been avoided with a log-in solution.

# Testing

For this project testing was not in focus. A manual e2e-testing is done in the form of alpha and beta testing.

## End-to-End Testing

The end-to-end was done manually, with a focus cover testing of different features and interactions that the user is offered. In this case all functionalities were tested and checked that the correct operation was activated when interacted e.g. load more, show more, refine search, rate, close. Testing did also include choosing different combinations of search, sort and filter and confirming that the right results occurred. This sort of testing was used to test the websites flow from start to end of a process. It has helped to simulate the real user scenarios and validate the system under test and its components for integration and data integrity.

## Alpha- and Beta Testing

The app is tested continuously on different phones and
Computers through emulators. The code has not taken into account that the application is to be used on an Android device, but it can be found in the code that some solutions have been implemented with _if android do this_ and _if iOS do this_. This was not completed through the code due to the lack of Android-OS-unit. The Beta-testing was an extended part of the end-to-end testing. The same operations were tested by different users. Two test persons were used
The test users both came up with a concrete one feedback that the buttons should be the same color and size to create a mutual association. This change was therefor implemented, resulting in the Refine button, being somewhat hidden. Unfortunately, a mistake was discovered during this faze. When testing the searchbar presented in the listview, or home page, the “done”/”ferdig”-button does not initialize the search. This problem might also occur for other devices or old versions of iOS. It was hard to fix this problem since it was working on the device used for testing.

# Structure

For this project the group used one of the most basic structures for a _MERN_ stack. The MERN stack includes MongoDB, Express, React, Node. The project's root folder contains a frontend and a backend folder. There are separate _package.json_ files and _node_modules_ for backend and frontend in order to differentiate the frontend/backend dependencies. On the backend, _Express_ was installed for the _Node_ runtime and _Mongoose_ for a more convenient way to talk to _MongoDB_. On the frontend, React was used for the frontend framework (native and typescript) and redux and hooks for state management. Express.js is a server-side web framework, and Node.js the popular and powerful JavaScript server platform. This was an ideal approach for working with JavaScrip, typescript and JSON, all the way through. The relationship is described as:

![Figure 1](pictures_documentation/Relationship.png)

For the folder composition
Two main sections were created: frontend and backend. Within frontend some subfolders are created with the most important being reducer folder (redux), types, assets and of course components. Assets include pictures and fonts that were used for the project. The components are decomposed into three main sections, head, body and footer. The folders that do not fit within those, are relevant for more than one folder and is placed outside. The body is divided into the actual view (lisview), extended view presenting the movie after show more is chosen and overlay – the different customizations that are available to refine the search h. The remining files in the body are components representing the header of the body, searchbar and refine-button. As mentioned, new components had to be introduced to fit the new layout for s smaller screened device, phones.

# How to Run

- Clone the repository with the HTTPS
- Enter the folder where expo-cammands can be run - "cd frontend"
- To install all packages that are used - "npm i"
- Start Metro Bundler and Expo DevTools on localhost - "expo start"
- Test application on emulator or expo application.

NB: As a user both devices needs to be connectet to the same WiFi.
The computer must not be connectet to a VPN when running starting expo start.
If not working run this command: source ~/.bash_profile

# Sources

- Disney image: https://www.pexels.com/nb-no/
- API calls: https://medium.com/weekly-webtips/patterns-for-doing-api-calls-in-reactjs-8fd9a42ac7d4
- React redux: https://www.youtube.com/watch?v=WpvIihorarA
- MERN stack: https://www.youtube.com/watch?v=7CqJlxBYj-M&t=1478s, https://www.mongodb.com/mern-stack
- Axios: https://medium.com/how-to-react/how-to-use-redux-in-your-react-app-with-axios-2327f581bf8a
- Inspiration and foundation: https://gitlab.stud.idi.ntnu.no/it2810-h20/team-63/project-3, https://gitlab.stud.idi.ntnu.no/it2810-h20/team-ad-hoc/project-4-h-s.git
- Introduction to start using react native: https://reactnative.dev/docs/getting-started
- Imported and most important third-party lib: https://reactnativeelements.com/
- To style rating text to match rating component: https://github.com/Monte9/react-native-ratings/blob/master/demo/src/TapRatingScreen.js

===============================================================================

Nikolai
