// Import react
import React, { useState } from "react";

// Import expo
import { StatusBar } from "expo-status-bar";
import { useFonts } from "@use-expo/font";
import { AppLoading } from "expo";

// Import react-native components
import {
  StyleSheet,
  ScrollView,
  SafeAreaView,
  Dimensions,
  View,
  Text,
  Image,
  TouchableHighlight,
  TextInput,
  FlatList,
  Keyboard,
} from "react-native";

// Local imports
/* import { Footer } from "./components/Footer"; */
import { Header } from "./components/header/Header";
/* import { Search } from "./components/search/Search" */
import { Search } from "./components/body/Search";
import { Footer } from "./components/footer/Footer";
import { BodyIOS } from "./components/body//listview/BodyIOS";
import Movies from "./components/body/listview/Movies";
import store from "./redux/store";
import { InteractionButton } from "./components/body/InteractionButton";

import { Provider } from "react-redux";
import { Body } from "./components/body/listview/Body";

export default function App() {
  //Use hook to load the font
  const [search, setSearch] = useState("null");

  const [isLoaded] = useFonts({
    Disney: require("./assets/fonts/waltograph42.ttf"),
  });
  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return (
      <View style={{ flex: 1 }}>
        {/*  Needed for storing */}
        <Provider store={store}>
          <ScrollView>
            <View style={styles.container}>
              <Header />
              <Search setSearch={setSearch} />
              <InteractionButton />
              <Movies />
              <StatusBar style="auto" />
            </View>
          </ScrollView>
          <View>
            <Footer />
          </View>
        </Provider>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    justifyContent: "center",
  },
});
