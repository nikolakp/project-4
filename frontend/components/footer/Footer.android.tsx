import React from "react";
import { View, Text, StyleSheet } from "react-native";

export const Footer = () => (
  <View style={styles.footer}>
    <Text style={styles.text}>
      Copyright &copy; 2020, Digital Movie Labrary
    </Text>
  </View>
);

const styles = StyleSheet.create({
  footer: {
    height: 50,
    backgroundColor: "#00f",
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    color: "#fff",
    fontSize: 24,
  },
});
