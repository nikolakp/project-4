import React from "react";
import { View, Text, StyleSheet } from "react-native";

export const Footer = () => (
  <View style={styles.footer}>
    <Text style={styles.text}>
      Copyright &copy; 2020, Digital Movie Labrary
    </Text>
  </View>
);

const styles = StyleSheet.create({
  footer: {
    height: 70,
    backgroundColor: "#191970",
    alignItems: "center",
    justifyContent: "center",
    position: "relative",
  },
  text: {
    color: "#fff",
    fontSize: 10,
  },
});
