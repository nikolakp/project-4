/***************************************************************************
 *                                                                          *
 *   Card                                                                   *
 *   - Displays the movies on a card                                        *
 *   - Includes a show more button to                                       *
 *                                                                          *
 ****************************************************************************/

import React, { useState, FunctionComponent } from "react";
import { Movie } from "../../types/movie";

import { Card, Text } from "react-native-elements"; //new import instead of react bootstrap
import { StyleSheet, View, Image, Platform } from "react-native"; //new import
import { ShowModal } from "./extendedview/Modal";
//new need to create

type WebCardProps = {
  movie: Movie;
};

//changed from functioncomp to react.fc
export const WebCard: FunctionComponent<WebCardProps> = ({ movie }) => {
  const [rating, setRating] = useState(movie.rating);
  return (
    <View style={styles.webCards}>
      <Card>
        <Image
          source={require("../../assets/walt-disney.jpeg")}
          style={{ width: 270, height: 180 }}
        />
        <Text>{"\n"}</Text>
        <Card.Title style={[styles.text, { fontSize: 18 }]}>
          {movie.movie_title}
        </Card.Title>
        <Text style={styles.text}>Genre: {movie.genre}</Text>
        <Text style={styles.text}>
          Rating: {rating} {"\n"}
        </Text>
        <ShowModal movie={movie} setRatingWebCard={setRating} />
        <Text style={styles.text2}>
          {"\n"} Movie attributes provided by: Prateek Majumder{" "}
        </Text>
      </Card>
    </View>
  );
};

const styles = StyleSheet.create({
  webCards: {
    marginTop: 30,
    flexGrow: 2,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-start",
    height: 400,
  },
  text: {
    fontFamily: Platform.OS === "ios" ? "Optima" : "sans-serif",
    textAlign: "center",
  },
  text2: {
    fontFamily: Platform.OS === "ios" ? "Optima" : "sans-serif",
    textAlign: "center",
    fontSize: 10,
  },
});
