import React, { useEffect, useRef, useState } from "react";

import {
  Dimensions,
  StyleSheet,
  View,
  TextInput,
  Text,
  Button,
  TouchableHighlight,
  Platform,
} from "react-native";

import { SearchBar } from "react-native-elements";
import { AnyIfEmpty, connect } from "react-redux";
import { InterModal } from "./overlay/InteractionModal";

/* import { searchBySlug, setPage } from "../store/search/actions";
import { selectPage } from "../store/search/selectors";
 */

/* import { faAngleUp, faMapMarkerAlt, faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
 */

type SearchProps = {
  setSearch: (search: string) => void;
  //modalStaus: (val: string) => void;
};

export const Search: React.FC<SearchProps> = ({ setSearch }) => {
  const [searchWord, setSearchWord] = useState("");

  //const [value, setValue] = useState("");
  //e=textinput
  // When user types in inputfield, update searchWord
  const handleChange = (e: string) => {
    setSearchWord(e);
    // If the user is not searching, search value is sat to null
    if (e === "") {
      setSearch("null");
    } else {
      setSearch(e);
    }
  };

  const debouncedSearchWord = useDebounce(searchWord, 400);

  /* useEffect(() => {
    if (props.page) {
      props.searchBySlug(searchWord, filters, props.page);
    }
  }, [props.page]);

  useEffect(() => {
    props.searchBySlug(debouncedSearchWord, filters, props.page);
  }, [debouncedSearchWord]);
 */
  //handles changes to the input field
  /* const handleChange = (e: string, prev: String) => {
    setSearchWord(e);
    if (!prev.includes(e) || e === "") {
      props.setPage(0);
    }
  }; */
  /*  const updateFilters = (filters: any) => {
    props.setPage(0);
    setFilters(filters);
  }; */

  {
    /*     <View style={styles.filter}>
        <Text style={styles.colorGray}>Sjanger</Text>
        <Text style={styles.colorGray}>Sortering</Text>
      </View> */
  }

  return (
    <View style={styles.searchtitle}>
      <SearchBar
        //lightTheme
        placeholder="Search for title..."
        returnKeyType="done"
        //onSubmitEditing = {(e) => handleClick(e)}
        onChangeText={(e) => handleChange(e)}
        style={styles.bar}
        value={searchWord}
        //fontColor="#c6c6c6"
        //iconColor="#c6c6c6"
        // shadowColor="#282828"
        //cancelIconColor="#c6c6c6"
        //inputStyle={{ backgroundColor: "red" }}
        // If this is true set style til dette setShowInterModal(ShowInterModal). else til
        containerStyle={
          Platform.OS === "android"
            ? {
                justifyContent: "center",
                height: 50,
                width: "100%",
                backgroundColor: "#4E4E4E",
              }
            : {
                justifyContent: "center",
                height: 60,

                width: "100%",
                backgroundColor: "#4E4E4E",
              }
        }
      />
    </View>
  );
};

//stolen from https://usehooks.com/useDebounce/
const useDebounce = (value: any, delay: any) => {
  const [debouncedValue, setDebouncedValue] = useState(value);

  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);

    return () => {
      clearTimeout(handler);
    };
  }, [value, delay]);
  return debouncedValue;
};

const styles = StyleSheet.create({
  searchtitle: {
    //width: Dimensions.get("window").width,
  },
  bar: {
    height: 40,
    //width: Dimensions.get("window").width,
    width: "100%",
    flex: 1,
  },
});
