/***************************************************************************
 *                                                                          *
 *   SortingBar                                                            *
 *   - Sets the correct sorting type and sort direction                     *
 *                                                                          *
 ****************************************************************************/
import React, { useState } from "react";
//import { Form } from 'react-bootstrap'
import { SORT } from "../../../types/interaction";
//import './SideBar.css

//New imports
import { CheckBox } from "react-native-elements";
import { View, Text, StyleSheet, ScrollView } from "react-native";

type SortProps = {
  setSortType: (type: string) => void;
  setSortDirection: (direction: number) => void;
};

// SearchBar inputs setSortType and setSortDirection function form sidebar,
// to update sort-state in sidebar
export const SortBar: React.FC<SortProps> = ({
  setSortType,
  setSortDirection,
}) => {
  // States to keep track of with sorting type that the user checks
  const [checkAZ, setCheckAZ] = useState(false);
  const [checkZA, setCheckZA] = useState(false);
  const [checkRatePos, setCheckRateInc] = useState(false);
  const [checkRateNeg, setCheckRateDec] = useState(false);
  const [checkNoSort, setCheckNoSort] = useState(true);

  // Function called when one sortType is checked
  const handleChecked = (
    setCheckState: (state: boolean) => void,
    sortType: { type: string; direction: number }
  ) => {
    // Set all checked false
    setCheckAZ(false);
    setCheckRateInc(false);
    setCheckZA(false);
    setCheckRateDec(false);
    setCheckNoSort(false);

    // Set the one just checked true
    setCheckState(true);

    // Update state in parent component
    setSortType(sortType.type);
    setSortDirection(sortType.direction);
  };

  //changed from divs and form to views and chekcbow
  return (
    <View>
      <Text style={styles.subtitle}>Sort</Text>
      <ScrollView style={styles.container}>
        <View style={styles.checkBox}>
          <CheckBox
            title={"No sort"}
            checked={checkNoSort}
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-o"
            onPress={() => handleChecked(setCheckNoSort, SORT.NO_SORT)}
          />
          <CheckBox
            title={"A to Z (title)"}
            checked={checkAZ}
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-o"
            onPress={() => handleChecked(setCheckAZ, SORT.TITLE_INC)}
          />
          <CheckBox
            title={"Z to A (titel)"}
            checked={checkZA}
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-o"
            onPress={() => handleChecked(setCheckZA, SORT.TITLE_DEC)}
          />
          <CheckBox
            title={"Best - Worst (rating)"}
            checked={checkRatePos}
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-o"
            onPress={() => handleChecked(setCheckRateInc, SORT.RATING_INC)}
          />
          <CheckBox
            title={"Worst - Best (rating"}
            checked={checkRateNeg}
            checkedIcon="dot-circle-o"
            uncheckedIcon="circle-o"
            onPress={() => handleChecked(setCheckRateDec, SORT.RATING_DEC)}
          />
        </View>
      </ScrollView>
    </View>
  );
};

//Css import

const styles = StyleSheet.create({
  subtitle: {
    fontSize: 20,
    marginBottom: 5,
    marginTop: 10,
  },
  container: {
    height: 200,
    borderWidth: 0.5,
    backgroundColor: "#4E4E4E",
  },
  checkBox: {
    marginVertical: 7,
  },
});
