/***************************************************************************
 *                                                                          *
 *     FilterBar                                                            *
 *     - Functionallity for filtering the movies                            *
 *                                                                          *
 ****************************************************************************/
import React, { useState } from "react";
import { View, Text, StyleSheet, ScrollView } from "react-native";
import { CheckBox } from "react-native-elements";

type FilterProps = {
  addFilter: (filters: string) => void;
  removeFilter: (filters: string) => void;
};

// SearchBar inputs add and remove filter functions form sidebar,
// and use these to update filter-list in sidebar
export const FilterBar: React.FunctionComponent<FilterProps> = ({
  addFilter,
  removeFilter,
}) => {
  // States to keep track of whish filters that are checked
  const [actionChecked, setActionChecked] = useState(false);
  const [adventureChecked, setAdventureChecked] = useState(false);
  const [documentaryChecked, setDocumentaryChecked] = useState(false);
  const [dramaChecked, setDramaChecked] = useState(false);
  const [horrorChecked, setHorrorChecked] = useState(false);
  const [musicalChecked, setMusicalChecked] = useState(false);
  const [comedyChecked, setComedyChecked] = useState(false);
  const [romanticChecked, setRomanticChecked] = useState(false);
  const [thrillerChecked, setThrillerChecked] = useState(false);

  type handleCheckedProps = {
    filter: string;
    state: boolean;
    setFunction: (state: boolean) => void;
  };

  // When a filter is checked or unchecked, change the checkstate
  // and update the filter list
  const handleChecked = ({
    filter,
    state,
    setFunction,
  }: handleCheckedProps) => {
    // If state is not checked
    if (!state) {
      // Set checkState to true
      setFunction(true);
      // Add filter to filter list
      addFilter(filter);
    }
    // If state is not checked
    else {
      // Set checkState to false
      setFunction(false);
      // Remove filter to filter list
      removeFilter(filter);
    }
  };
  //Converted from project 3 to React Native syntax. Form.Check now changed to CheckBox. onPress to onChange and added styling.

  return (
    <View>
      <Text style={styles.subtitle}>Filter your search </Text>
      <ScrollView style={styles.container}>
        {/* type='checkbox'
                id={'ActionCheckbox'}
                label={'Action'}
                checked={actionChecked}
                onChange={() => handleChecked({filter:'Action', state: actionChecked, setFunction: setActionChecked})} */}
        <View style={styles.checkBox}>
          <CheckBox
            title={"Action"}
            checked={actionChecked}
            onPress={() =>
              handleChecked({
                filter: "Action",
                state: actionChecked,
                setFunction: setActionChecked,
              })
            }
          />

          <CheckBox
            title={"Adventure"}
            checked={adventureChecked}
            onPress={() =>
              handleChecked({
                filter: "Adventure",
                state: adventureChecked,
                setFunction: setAdventureChecked,
              })
            }
          />

          <CheckBox
            title={"Documentary"}
            checked={documentaryChecked}
            onPress={() =>
              handleChecked({
                filter: "Documentary",
                state: documentaryChecked,
                setFunction: setDocumentaryChecked,
              })
            }
          />

          <CheckBox
            title={"Comedy"}
            checked={comedyChecked}
            onPress={() =>
              handleChecked({
                filter: "Comedy",
                state: comedyChecked,
                setFunction: setComedyChecked,
              })
            }
          />

          <CheckBox
            title={"Drama"}
            checked={dramaChecked}
            onPress={() =>
              handleChecked({
                filter: "Drama",
                state: dramaChecked,
                setFunction: setDramaChecked,
              })
            }
          />

          <CheckBox
            title={"Horror"}
            checked={horrorChecked}
            onPress={() =>
              handleChecked({
                filter: "Horror",
                state: horrorChecked,
                setFunction: setHorrorChecked,
              })
            }
          />

          <CheckBox
            title={"Musical"}
            checked={musicalChecked}
            onPress={() =>
              handleChecked({
                filter: "Musical",
                state: musicalChecked,
                setFunction: setMusicalChecked,
              })
            }
          />

          <CheckBox
            title={"Romantic Comedy"}
            checked={romanticChecked}
            onPress={() =>
              handleChecked({
                filter: "Romantic Comedy",
                state: romanticChecked,
                setFunction: setRomanticChecked,
              })
            }
          />

          <CheckBox
            title={"Thriller/Suspense"}
            checked={thrillerChecked}
            onPress={() =>
              handleChecked({
                filter: "Thriller",
                state: thrillerChecked,
                setFunction: setThrillerChecked,
              })
            }
          />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  subtitle: {
    fontSize: 20,
    marginBottom: 5,
    marginTop: 10,
  },
  checkBox: {
    marginVertical: 7,
  },
  container: {
    height: 200,
    borderWidth: 0.5,
    backgroundColor: "#4E4E4E",
  },
});
