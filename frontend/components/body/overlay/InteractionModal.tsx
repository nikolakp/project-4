/***************************************************************************
 *                                                                          *
 *  THIS IS THE OLD SideBar NOW Implemented as a pop-UP MODAL               *
 *   - Holds filterBar, searchBar and sortBar                               *
 *   - Fetch movies from mongoDB depending on filter, search and sort-state *
 *                                                                          *
 ****************************************************************************/

import React, { useState } from "react";
import { useDispatch } from "react-redux";

import { FilterBar } from "./FilterBar";
import { Search } from "../Search";

import mainPath from "../../../utils";
import axios from "axios";
import {
  errorOnFetch,
  getResult,
  updatePath,
  setPage,
} from "../../../redux/actions";
//import "./SideBar.css";

import { View, StyleSheet } from "react-native";
import { Button, SearchBar } from "react-native-elements";
import { SortBar } from "./SortBar";

//Must create props for vis and unvis- interactionmodal
type InterModalProps = {
  setShowInterModal: () => void;
  setSearchWord: (message: string) => void;
};

export const InterModal: React.FC<InterModalProps> = ({
  setShowInterModal,
  setSearchWord,
}) => {
  // States for saving the search, filter and sort values
  const [filters, setFilters] = useState(["null"]);
  const [search, setSearch] = useState("null");

  const [sortType, setSortType] = useState("release_date");
  const [sortDirection, setSortDirection] = useState(1);

  // Makes i possible to use dispatch
  const dispatch = useDispatch();

  // Functionallity for when show results is pressed
  // Fetch correct movies depending on filter, serach and sort states
  const handleClick = async () => {
    try {
      // Create path to fetch from
      const path =
        mainPath +
        `filter/${filters}/sort/${sortType}/${sortDirection}/search/${search}/page/0`;

      // Update the global path and page
      // This is necessary for the load more functionallity
      dispatch(updatePath(path));
      dispatch(setPage(0));

      // Get the movies corresponing with the filter, search and sort states
      const res = await axios.get(path);

      // Dispatch them to the global state
      dispatch(getResult(res.data));
      setShowInterModal(); //this is new from old code.
    } catch (e) {
      dispatch(errorOnFetch("Error when fetching movies"));
    }
  };

  // Add new filter, if a filter is checked
  // Function is passed to FilterBar as props
  const addFilter = (filter: string) => {
    var newFilters: string[] = filters.slice();
    newFilters.push(filter);
    setFilters(newFilters);
  };

  // Remove new filter, if a filter is checked
  // Function is passed to FilterBar as props
  const removeFilter = (filter: string) => {
    var newFilters: string[] = filters.slice().filter((f) => f !== filter);
    setFilters(newFilters);
  };

  return (
    <View style={styles.interModalContainer}>
      <View style={styles.newSearch}>
        <Search setSearch={setSearch}></Search>
      </View>
      <View>
        <FilterBar addFilter={addFilter} removeFilter={removeFilter} />
      </View>
      <View>
        <SortBar
          setSortType={setSortType}
          setSortDirection={setSortDirection}
        />
        <Button
          style={styles.resbut}
          onPress={() => handleClick()}
          title="Refine results"
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  newSearch: {},
  interModalContainer: {
    marginTop: 5,
    marginBottom: 5,
    marginHorizontal: 10,

    //backgroundColor: "gray"
  },
  resbut: {
    marginTop: 10,
    alignSelf: "center",
    width: 350,
  },
});
