import React from "react";
import { View, Text } from "react-native";
import { styles } from "../../../theme/Styles";

export const BodyAndroid = () => (
  <View style={styles.body}>
    <Text style={styles.h1}>This is Android App!</Text>
  </View>
);
