/***************************************************************************
 *                                                                          *
 *     LoadMore                                                             *
 *     - LoadMore button for loading more movies from mongoDB               *
 *                                                                          *
 ****************************************************************************/

// Import requiered content
import React from "react";
import axios from "axios";
import { useSelector, useDispatch } from "react-redux";
import { Movie, MovieState } from "../../../types/movie";
import { Button } from "react-native-elements";
import { View, StyleSheet, Platform } from "react-native";
import { moviesPerPage } from "../../../utils";
import { errorOnFetch, loadMovies, setPage } from "../../../redux/actions";

// removed unessesary functionallity to avoid automatic scrolling on load more

export function LoadMore() {
  // Get path, page and movies from the global state
  const path: string = useSelector((state: MovieState) => state.path);
  const page: number = useSelector((state: MovieState) => state.page);
  const movies: Movie[] = useSelector((state: MovieState) => state.movies);

  // Make it possible to use dispatch
  const dispatch = useDispatch();

  // Check if there are more movies to be loaded
  const noMoreMovies = () => {
    // If the current list is less than movies per page,
    // there are no more movies matching the query
    if (movies.length >= moviesPerPage) {
      return false;
    }
    return true;
  };

  // Functionallity for when load more button is clicked
  const handleClick = async () => {
    try {
      // get the path to load from, and change the page number
      const getPath = path.slice(0, -2) + `/${page + 1}`;

      // Get the next movies, and dispath them
      const res = await axios.get(getPath);
      dispatch(loadMovies(res.data));

      // dispatch the new page number
      dispatch(setPage(page + 1));
    } catch (e) {
      // If there is an error dispatch errorOnFetch
      dispatch(errorOnFetch("Error when fetching movies"));
    }
  };

  // The only change for this file is the View instead of divs.
  return (
    <View>
      <Button
        style={styles.bottombutton}
        title="Load more movies..."
        disabled={noMoreMovies()}
        onPress={() => handleClick()}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  bottombutton: {
    fontFamily: Platform.OS === "ios" ? "Optima-Bold" : "sans-serif",
    textAlign: "center",
    marginTop: 20,
  },
});
