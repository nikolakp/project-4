/***************************************************************************
 *                                                                          *
 *     Movies                                                               *
 *     - Maps all movies to movieCards and fetches nine                     *
 *       movies when the user fist visits the page                          *
 *                                                                          *
 ****************************************************************************/
import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Movie, MovieState } from "../../../types/movie";
import { getTenItems, errorOnFetch } from "../../../redux/actions";
import { WebCard } from "../WebCard";
import { LoadMore } from "./LoadMore";

import { View, Text, Platform, StyleSheet } from "react-native"; //react natice comps

export default function Movies() {
  /*   NEW */
  const [load, setLoad] = useState(true); //new state
  // Get movies from global state
  /*   NEW */
  const movies: Movie[] = useSelector((state: MovieState) => state.movies);

  // Make it possible to use dispatch
  const dispatch = useDispatch();

  // Get first default movies
  const getFirstMovies = async () => {
    try {
      dispatch(await getTenItems(0));
    } catch (e) {
      dispatch(
        errorOnFetch(
          "components/movies/Movies.tsx: There was error while fetching movies"
        )
      );
    }
  };

  // On refresh get first default movies

  /*   NEW */
  if (load) {
    getFirstMovies();
    setLoad(false);
  }
  /*   NEW */

  return (
    <View>
      <View>
        {movies.length === 0 && (
          <Text style={styles.errorText}>No matches for your search</Text>
        )}
        {movies.map((movie) => (
          <WebCard movie={movie} key={movie._id} />
        ))}
      </View>
      <LoadMore />
    </View>
  );
}

const styles = StyleSheet.create({
  errorText: {
    fontFamily: Platform.OS === "ios" ? "Optima-Bold" : "sans-serif",
    textAlign: "center",
  },
});
