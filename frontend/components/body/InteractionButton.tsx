import React, { useState } from "react";
import { Dimensions, StyleSheet, View } from "react-native";
import { Overlay } from "react-native-elements";
import { InterModal } from "./overlay/InteractionModal";
import { Button } from "react-native-elements";

export const InteractionButton = () => {
  const [searchWord, setSearchWord] = useState("");
  const [ShowInterModal, setShowInterModal] = useState(false);
  const toggleInterModal = () => {
    setShowInterModal(!ShowInterModal);
  };

  return (
    <View>
      <Button
        style={styles.buttonBar}
        title="Advanced search"
        onPress={toggleInterModal}
      />

      <Overlay
        isVisible={ShowInterModal}
        overlayStyle={styles.overlay}
        onBackdropPress={toggleInterModal}
      >
        <InterModal
          setShowInterModal={toggleInterModal}
          setSearchWord={setSearchWord}
        />
      </Overlay>
    </View>
  );
};

const styles = StyleSheet.create({
  buttonBar: {
    /* backgroundColor: "gray", */
    marginTop: 10,

    alignSelf: "center",
    //width: Dimensions.get("window").width,
  },
  overlay: {
    margin: 20,
    width: 350,
    height: 610,
  },
});
