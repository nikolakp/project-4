import React, { FunctionComponent, useState } from "react";
import { Movie } from "../../../types/movie";

import { Overlay, Button } from "react-native-elements"; //Modal is Overlay in react-native
import { Platform, View, Text, StyleSheet, Image } from "react-native";

// Include starRating for rating when modal is shown
import { StarRating } from "./StarRating"; //ikke laget enda

type ModalProps = {
  movie: Movie;
  setRatingWebCard: (rating: number) => void;
};

export const ShowModal: React.FC<ModalProps> = ({
  movie,
  setRatingWebCard,
}) => {
  const [visible, setVisible] = useState(false);
  const toggleModal = () => {
    setVisible(!visible);
  };

  const [numberOfRatings, setNumberOfRatings] = useState(
    movie.number_of_ratings
  );

  //buttons title is the text for the button
  return (
    <>
      <View>
        <Button title="Show more" onPress={toggleModal} />

        <Overlay isVisible={visible} onBackdropPress={toggleModal}>
          <View style={styles.modalContainer}>
            <Text style={styles.title}>{movie.movie_title}</Text>
            <Text> </Text>

            <Image
              source={require("../../../assets/walt-disney.jpeg")}
              style={{ width: 250, height: 150 }}
            ></Image>
            <Text style={styles.movieInfo}>Genre: {movie.genre}</Text>
            <Text style={styles.movieInfo}>
              The release date: {movie.release_date}
            </Text>
            <StarRating
              movie={movie}
              setNumberOfRatings={setNumberOfRatings}
              setRatingWebCard={setRatingWebCard}
            />
            <Text style={styles.movieInfo}>
              The number of raings: {numberOfRatings}
            </Text>
            <Button
              title="Close"
              style={{ width: 300 }}
              onPress={toggleModal}
            />
            <Text style={styles.text2}>{"\n"} Click the stars to rate </Text>
          </View>
        </Overlay>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 30,
    textAlign: "center",
    //fontFamily: "Disney",
    fontFamily: Platform.OS === "ios" ? "Disney" : "Disney",
  },
  modalContainer: {
    height: 440,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
  },
  movieInfo: {
    padding: 3,
    color: "black",
    fontFamily: Platform.OS === "ios" ? "Optima" : "sans-serif",
    fontSize: 16,
  },
  text2: {
    fontFamily: Platform.OS === "ios" ? "Optima" : "sans-serif",
    textAlign: "center",
    fontSize: 10,
  },
});
