/***************************************************************************
 *                                                                          *
 *     StarRating                                                           *
 *     - Functionallity for rating movies                                   *
 *                                                                          *
 ****************************************************************************/

import React, { useState } from "react";
import axios from "axios";
import { useDispatch } from "react-redux";
import { Movie } from "../../../types/movie";
import { errorOnFetch, updateRating } from "../../../redux/actions";

//NEW IMPORTS
import mainPath from "../../../utils";
import { AirbnbRating } from "react-native-elements"; //Got this from the former group. That used the same.
import { View, Text, StyleSheet, Platform } from "react-native";

type StarProps = {
  movie: Movie;
  //setraiting was removed from original project 3.
  setNumberOfRatings: (number_of_ratings: number) => void;
  setRatingWebCard: (rating: number) => void;
};

export const StarRating: React.FC<StarProps> = ({
  movie,
  setNumberOfRatings,
  setRatingWebCard,
}) => {
  const [ratingLocal, setRatingLocal] = useState(movie.rating);
  const dispatch = useDispatch();
  const [userRating, setUserRating] = useState(0);

  // Updates the movie rating
  const updateRatingMovie = async (rating: number) => {
    setUserRating(rating);
    try {
      let newRating = 0;
      let newNumberOfRatings = 0;

      // New rating is the caluladed average rating of all ratings
      newRating =
        Math.round(
          ((movie.rating * movie.number_of_ratings + rating) /
            (movie.number_of_ratings + 1)) *
            10
        ) / 10;
      newNumberOfRatings = movie.number_of_ratings + 1;

      // Update the rating in mongoDB
      //changed path to mainPath: http://${it2810-63.idi.ntnu.no}:${3000}/movies/
      const sucess = await axios.post(`${mainPath}updateRating/${movie._id}`, {
        rating: newRating,
        number_of_ratings: newNumberOfRatings,
      });
      if (sucess) {
        // Get the updated movie on id and dispatch it to global state
        setRatingLocal(newRating);
        setRatingWebCard(newRating);
        setNumberOfRatings(newNumberOfRatings);
        const res = await axios.get(`${mainPath}${movie._id}`);
        dispatch(updateRating([res.data]));
      }
    } catch (e) {
      dispatch(
        errorOnFetch(
          `components/movies/Movies.tsx: There was error while updating movie: ${movie._id}`
        )
      );
    }
  };

  return (
    <View style={styles.ratingInfo}>
      <Text style={styles.ratingText}>Total rating: {ratingLocal}/10</Text>

      <Text style={[styles.ratingText2]}>
        Your latest rating: {userRating}/10
      </Text>

      <View>
        <AirbnbRating //this is an import almost the same as Rating from Rreact-Rating
          count={10}
          defaultRating={userRating}
          size={25}
          onFinishRating={(rating) => updateRatingMovie(rating)}
          showRating={false}
        />
      </View>
    </View>
  );
};

// This style is gotten from
// https://github.com/Monte9/react-native-ratings/blob/master/demo/src/TapRatingScreen.js
// to make the text match the star rating design.
const styles = StyleSheet.create({
  ratingInfo: {
    flexDirection: "column",
    alignItems: "center",
  },
  ratingText: {
    fontWeight: "normal",
    fontFamily: Platform.OS === "ios" ? "Optima-Bold" : "sans-serif",
    margin: 5,
  },
  ratingText2: {
    fontWeight: "normal",
    fontFamily: Platform.OS === "ios" ? "Optima-Bold" : "sans-serif",
    margin: 5,
    color: "gold",
  },
});
