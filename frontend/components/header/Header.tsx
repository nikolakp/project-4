import React from "react";
import { View, Text, StyleSheet, Platform } from "react-native";

export const Header = () => (
  <View style={styles.header}>
    <Text style={styles.text}>Disney Movies</Text>
  </View>
);

/* Platform.select will return the value given the key from Platform.OS., so in this case the code becomes with no top padding for Android.*/

const styles = StyleSheet.create({
  header: {
    height: Platform.OS === "android" ? 76 : 100,
    marginTop: Platform.OS === "ios" ? 0 : 24,
    ...Platform.select({
      ios: { backgroundColor: "#191970", paddingTop: 24 },
      android: { backgroundColor: "#191970" },
    }),
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    color: "#fff",
    fontSize: 24,
    fontFamily: "Disney",
    textAlign: "center",
  },
});
