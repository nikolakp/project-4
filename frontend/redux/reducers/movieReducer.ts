/***************************************************************************
 *                                                                          *
 *   MovieReducer                                                           *
 *   - Defines the reducer for movielist, path and page                     *
 *                                                                          *
 ****************************************************************************/

import { Action, MovieState } from "../../types/movie";
import { Reducer } from "redux";
import { MOVIE } from "../actionTypes";
import mainPath from "../../utils";

// InitialState for when the page is loaded
//ONLY CHANGED THIS PART ON THIS FILE
const initialState = {
  movies: [],
  path: `${mainPath}filter/null/sort/movie_title/1/search/null/page/1`,
  page: 0,
};

// TO THIS PART

const movieReducer: Reducer<MovieState, Action> = (
  state: MovieState = initialState,
  action: Action
) => {
  switch (action.type) {
    case MOVIE.GET_RESULT:
      if (
        typeof action.payload === "string" ||
        typeof action.payload === "number"
      ) {
        return state;
      }
      state = { ...state, movies: [], path: state.path };
      for (let item of action.payload) {
        state = { ...state, movies: [...state.movies, item], path: state.path };
      }
      console.log(state.movies);
      return state;
    case MOVIE.GET_TEN:
      if (
        typeof action.payload === "string" ||
        typeof action.payload === "number"
      ) {
        return state;
      }
      for (let item of action.payload) {
        state = { ...state, movies: [...state.movies, item], path: state.path };
      }
      return state;
    case MOVIE.UPDATE_PATH:
      if (
        typeof action.payload !== "string" &&
        typeof action.payload !== "number"
      ) {
        return state;
      }
      return { ...state, movies: state.movies, path: action.payload };
    case MOVIE.SET_PAGE:
      if (typeof action.payload !== "number") {
        return state;
      }
      return {
        ...state,
        movies: state.movies,
        path: state.path,
        page: action.payload,
      };
    case MOVIE.LOAD_MORE_MOVIES:
      if (
        typeof action.payload === "string" ||
        typeof action.payload === "number"
      ) {
        return state;
      }
      for (let item of action.payload) {
        state = { ...state, movies: [...state.movies, item], path: state.path };
      }
      console.log(state.movies);
      return state;
    case MOVIE.UPDATE_RATING:
      if (
        typeof action.payload === "string" ||
        typeof action.payload === "number"
      ) {
        return state;
      }
      const updatedMovie = action.payload[0];
      const movieList = state.movies;
      const movieIndex = movieList.findIndex(
        (movie) => movie._id === updatedMovie._id
      );
      movieList[movieIndex].rating = updatedMovie.rating;
      movieList[movieIndex].number_of_ratings = updatedMovie.number_of_ratings;
      return { ...state, movies: movieList };
    case MOVIE.ERROR:
      return state;
    default:
      return state;
  }
};

export default movieReducer;
