/***************************************************************************
*                                                                          *
*   actionsTypes                                                           *
*   - Defines all actions types    is                                      *
*                                                                          *
****************************************************************************/
export const MOVIE = {
  GET_TEN: 'GET_TEN',
  UPDATE_PATH: 'UPDATE_PATH',
  LOAD_MORE_MOVIES: 'LOAD_MORE_MOVEIS',
  UPDATE_RATING: 'UPDATE_RATING',
  GET_RESULT: 'GET_RESULT',
  SET_PAGE: 'SET_PAGE',
  ERROR: 'ERROR'
}
