/***************************************************************************
 *                                                                          *
 *   actions                                                                *
 *   - Defines all actions that is dispathced                               *
 *                                                                          *
 ****************************************************************************/
import { MOVIE } from "./actionTypes";
import { Movie } from "../types/movie";
import axios from "axios";
import mainPath from "../utils";

// Action for updating the movies
export const getResult = (movies: Movie[]) => ({
  type: MOVIE.GET_RESULT,
  payload: movies,
});

// Action for getting default movies
export const getTenItems = async (page: number) => {
  return await axiosGet(`/getMovies/${page}`, MOVIE.GET_TEN);
};

// Action for updating the path
export const updatePath = (path: string) => ({
  type: MOVIE.UPDATE_PATH,
  payload: path,
});

// Action for updating the page
export const setPage = (page: number) => ({
  type: MOVIE.SET_PAGE,
  payload: page,
});

// Action for loading more movies
export const loadMovies = (movies: Movie[]) => {
  return {
    type: MOVIE.LOAD_MORE_MOVIES,
    payload: movies,
  };
};

// Action for updating the rating of one movie
export const updateRating = (movies: Movie[]) => {
  return {
    type: MOVIE.UPDATE_RATING,
    payload: movies,
  };
};

// Action for when there is an error
export const errorOnFetch = (err: string) => ({
  type: MOVIE.ERROR,
  payload: err,
});

// Gets default movies
const axiosGet = async (path: string, type: string) => {
  try {
    const res = await axios.get(mainPath + path);
    return {
      type: type,
      payload: res.data,
    };
  } catch (e) {
    return errorOnFetch(`There was error while fetching movies`);
  }
};
