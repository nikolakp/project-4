const vmAdress = "it2810-63.idi.ntnu.no";
const backendPort = 3000;
const mainPath = `http://${vmAdress}:${backendPort}/movies/`;
export default mainPath;
export const moviesPerPage = 9;
