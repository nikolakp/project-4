/***************************************************************************
*                                                                          *
*   movie                                                                  *
*   - Defines Action and MovieState used in reducer                        *
*   - Defines the movie interface                                          *
*                                                                          *
****************************************************************************/
export interface Movie {
    _id: string,
    movie_title: string,
    release_date: string,
    genre: string,
    rating: number,
    number_of_ratings: number
  }

export type Action = {
    type: string,
    payload: Movie[] | string
}

export interface MovieState {
    movies: Movie[],
    path: string,
    page: number
  }