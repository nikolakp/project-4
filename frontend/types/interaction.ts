/***************************************************************************
*                                                                          *
*   interaction                                                            *
*   - Defines all interaction types for sorting and filter                 *
*                                                                          *
****************************************************************************/
export const SORT = {
  TITLE_INC: { type: 'movie_title', direction: 1 },
  TITLE_DEC: { type: 'movie_title', direction: -1 },
  RATING_INC: { type: 'rating', direction: -1 },
  RATING_DEC: { type: 'rating', direction: 1 },
  NO_SORT: { type: 'release_date', direction: 1 }
}

export const FILTER = {
  ACTION: 'Action',
  ADVENTURE: 'Adventure',
  DOCUMENTARY: 'Documentary',
  COMEDY: 'Comedy',
  DRAMA: 'Drama',
  HORROR: 'Horror',
  MUSICAL: 'Musical',
  ROMANTIC_COMEDY: 'Romantic Comedy',
  THRILLER: 'Thriller'
}
