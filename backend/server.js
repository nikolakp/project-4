/***************************************************************************
*                                                                          *
*     Server                                                               *
*     - Creates the server                                                 *
*                                                                          *
****************************************************************************/

// Import express, cors and mongoose
const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')

const app = express() // Init express
const port = 5000 // Set port to connect to

app.use(cors()) // Alow cross-origin
app.use(express.json())

// Connect to mongodb
const uri = 'mongodb://admin:admin@it2810-63.idi.ntnu.no:27017/disney?authSource=admin&readPreference=primary'
mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true })

const connection = mongoose.connection
connection.once('open', () => {
  console.log('MongoDB is connected')
})

// Import the movie router
const movieRouter = require('./routes/movies')

app.use('/movies', movieRouter)

// Listen on port
app.listen(port, () => {
  console.log(`Server running on port: ${port}`)
})
