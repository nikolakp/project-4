/***************************************************************************
 *                                                                          *
 *     Movies                                                               *
 *     - Defines all routes and functionallity for geting moveis from       *
 *       mongoDB                                                            *
 *                                                                          *
 ****************************************************************************/

// Import express router and movie model
const router = require("express").Router();
const Movie = require("../models/movie.model");

// Define amount of movies per page
const moviesPerPage = 9;

// Get not sorted movies for the specific page
router.route("/getMovies/:page").get((req, res) => {
  Movie.find()
    .skip(req.params.page * moviesPerPage)
    .limit(moviesPerPage)
    .then((movie) => res.json(movie))
    .catch((err) => res.status(400).json("Error " + err));
});

// Get movie by id
router.route("/:id").get((req, res) => {
  Movie.findById(req.params.id)
    .then((movie) => res.json(movie))
    .catch((err) => res.status(400).json("Error " + err));
});

// update rating for movie, by id
router.route("/updateRating/:id").post((req, res) => {
  Movie.findById(req.params.id)
    .then((movie) => {
      movie.rating = req.body.rating;
      movie.number_of_ratings = req.body.number_of_ratings;

      movie
        .save()
        .then(() => res.json("Movie updated!"))
        .catch((err) => res.status(400).json("Error: " + err));
    })
    .catch((err) => res.status(400).json("Error " + err));
});

// Get movies using filter, sorting and search
router
  .route("/filter/:filter/sort/:sortType/:dircetion/search/:search/page/:page")
  .get((req, res) => {
    // Extract filers from path
    // Req.params.filter is defined filter1,filter2, so we need to split on ','
    let filters = req.params.filter.split(",");

    // First define all filter queries
    const filterQueries = [];

    // If no filters, include all genres
    if (req.params.filter === "null") {
      filters = [
        "Adventure",
        "Action",
        "Documentary",
        "Comedy",
        "Drama",
        "Horror",
        "Musical",
        "Romantic Comedy",
        "Thriller",
      ];
    }
    // Else the filter is defined, and a query for each filter is created
    for (var filter of filters) {
      if (filter !== "null") {
        filterQueries.push({ genre: { $regex: `^${filter}`, $options: "i" } });
      }
    }

    // Creating the sort query depending on the sortType and dircetion
    // Default is on release_date increasing
    let sortQuery = { release_date: req.params.dircetion };
    if (req.params.sortType === "rating") {
      sortQuery = { rating: req.params.dircetion };
    }
    if (req.params.sortType === "movie_title") {
      sortQuery = { movie_title: req.params.dircetion };
    }

    // If the user is searching for something a searchquery is created
    if (req.params.search !== "null") {
      const searchQuery = {
        movie_title: { $regex: `${req.params.search}`, $options: "i" },
      };
      Movie.find({ $and: [{ $or: filterQueries }, searchQuery] })
        .sort(sortQuery)
        .skip(req.params.page * moviesPerPage)
        .limit(moviesPerPage)
        .then((movie) => res.json(movie))
        .catch((err) => res.status(400).json("Error " + err));
    } else {
      // Find movie with matching filter
      Movie.find({ $or: filterQueries })
        .sort(sortQuery)
        .skip(req.params.page * moviesPerPage)
        .limit(moviesPerPage)
        .then((movie) => res.json(movie))
        .catch((err) => res.status(400).json("Error " + err));
    }
  });

// Export router
module.exports = router;
