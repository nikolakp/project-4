/****************************************************************************
*                                                                           *
*     Movie.model                                                           *
*     - Defines movieSchema                                                 *
*                                                                           *
****************************************************************************/

// Import mongoose
const mongoose = require('mongoose')

// Define Schema
const Schema = mongoose.Schema

// Define MovieSchema
const movieSchema = new Schema({
  movie_title: {
    type: String,
    required: true
  },
  release_date: {
    type: String,
    required: true
  },
  genre: {
    type: String,
    required: true
  },
  number_of_ratings: {
    type: Number,
    required: false
  },
  rating: {
    type: Number,
    required: false
  }
})

// Define and export movie model
const Movie = mongoose.model('Movie', movieSchema)

module.exports = Movie
